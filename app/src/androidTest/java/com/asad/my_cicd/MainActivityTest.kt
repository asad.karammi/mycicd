package com.asad.my_cicd

import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @get:Rule
    val intentsTestRule = IntentsTestRule(MainActivity::class.java)
    private lateinit var textView: ViewInteraction
    private lateinit var button:ViewInteraction

    @Before
    fun before(){
        textView = Espresso.onView(ViewMatchers.withId(R.id.textView))
        button = Espresso.onView(ViewMatchers.withId(R.id.button))
    }

    @Test
    fun checkChangeTextAfterPressedButton(){

        Thread.sleep(1000)
        button.perform(ViewActions.click())
        Thread.sleep(500)
        textView.check(ViewAssertions.matches(ViewMatchers.withText("change")))

    }

}